<?php

class Tigren_DailyDeal_Block_Adminhtml_DailyDeal_Edit_Tab_DailyDeal extends Mage_Adminhtml_Block_Widget_Form
{

    private $dataProduct;

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('dailydeal_form', array('legend' => Mage::helper('dailydeal')->__('Daily Deal Information')));

        $this->dataProduct = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addAttributeToFilter('entity_id', 232);
        $product = [];
        foreach ($this->dataProduct as $item)
            $product = $item->getData();

        $fieldset->addField('entity_id', 'text', array(
            'label' => 'Product Name',
            'name' => 'entity_id',
            'disabled' => true,
        ));

        $fieldset->addField('product_price', 'text', array(
            'label' => 'Product Price',
            'name' => 'product_price',
            'disabled' => true,
        ));

        $fieldset->addField('deal_name', 'text', array(
            'label' => 'Name',
            'name' => 'deal_name',
            'class' => 'required-entry',
            'required' => true,
        ));

        $fieldset->addField('deal_start', 'datetime', array(
            'label' => 'From',
            'name' => 'deal_start',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM)
        ));

        $fieldset->addField('deal_end', 'datetime', array(
            'label' => 'To',
            'name' => 'deal_end',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM)
        ));

        $fieldset->addField('deal_price', 'text', array(
            'label' => 'Price',
            'name' => 'deal_price',
            'class' => 'required-entry',
        ));

        $fieldset->addField('deal_price_type', 'select', array(
            'label' => 'Daily Deak Type',
            'name' => 'deal_price_type',
            'options' => ['0' => 'Sale Off', '1' => 'Price Only'],
        ));

        $fieldset->addField('deal_qty', 'text', array(
            'label' => 'Quantity',
            'name' => 'deal_qty',
            'class' => 'required-entry',
            'required' => true,
        ));

        $fieldset->addField('deal_qty_sold', 'text', array(
            'label' => 'Quantity Sold',
            'name' => 'deal_qty_sold',
            'class' => 'required-entry',
            'required' => true,
            'disabled' => true,
        ));

        $fieldset->addField('stores', 'multiselect', array(
            'name'      => 'stores[]',
            'label'     => 'Select Store',
            'title'     => 'Select Store',
            'required'  => true,
            'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(true, true),
        ));

        $fieldset->addField('status', 'select', array(
            'label' => 'Is Active',
            'name' => 'status',
            'options' => ['1' => 'Enable', '0' => 'Disabled'],
        ));


        if (Mage::getSingleton('adminhtml/session')->getDailydealData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getDailydealData());
            Mage::getSingleton('adminhtml/session')->getDailydealData(null);
        } elseif (Mage::registry('dailydeal_data')) {
            $form->setValues(Mage::registry('dailydeal_data')->getData());
        }

        $this->setForm($form);
        return parent::_prepareForm();
    }

}
