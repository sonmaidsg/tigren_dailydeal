<?php
class Tigren_DailyDeal_Block_Adminhtml_DailyDeal_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
        $this->setUseAjax(true); // Using ajax grid is important
        $this->setDefaultSort('entity_id');
        if($this->getDailyDeal() && $this->getDailyDeal()->getId()){
            $this->setDefaultFilter(array('in_deal' => ''));
        }
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareColumns() {

        $this->addColumn('in_deal', array(
            'header_css_class'  => 'a-center',
            'align'             => 'center',
            'header'            => 'Select',
            'type'              => 'radio',
            'html_name'         => 'in_deal',
            'values'            => $this->_getSelectedProduct(),
            'index'             => 'entity_id',
        ));

        $this->addColumn('name', array(
            'header'=> 'Name',
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('price', array(
            'header'=> 'Price',
            'index' => 'price',
            'type'  => 'price',
            'currency_code' => Mage::app()->getStore()->getBaseCurrency()->getCode()
        ));

        return parent::_prepareColumns();
    }

    protected function _getSelectedProduct()
    {
        $person = array_keys($this->getSelectedProduct());
        return $person;
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
//            ->addFieldToFilter('entity_id', array('in' => $this->_getSelectedProduct()))
            ->addAttributeToSelect(['name', 'price']);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getSelectedProduct()
    {
        $currentDealId = $this->getRequest()->getParam('deal_id');
        if(!isset($currentDealId)) {
            $currentDealId = 0;
        }
        $product = Mage::getModel('dailydeal/dailydeal')->load($currentDealId)->getData();
        $data = [];

        $data[$product['entity_id']] = ['deal_id' => $product['entity_id']];

        return $data;
    }


    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/productgrid', array('_current'=>true));
    }

    protected function _addColumnFilterToCollection($column)
    {

        if ($column->getId() == 'in_deal') {
            $productId = $this->_getSelectedProduct();
            if (empty($productId)) {
                $productId = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productId));
            } else {
                if($productId) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productId));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    public function getDailyDeal(){
        return Mage::getModel('dailydeal/dailydeal');
    }

    public function getRowUrl($item)
    {
        return "javascript:void()";
    }

}?>