<?php

class Tigren_DailyDeal_Block_Adminhtml_DailyDeal_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct() {
        parent::__construct();
        $this->setId('dailydeal_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('dailydeal')->__('Daily Deal Information'));
    }

    protected function _beforeToHtml() {

        $this->addTab('product', array(
            'label' => Mage::helper('dailydeal')->__('Chooser Product'),
            'alt' => Mage::helper('dailydeal')->__('Chooser Product'),
            'url' => $this->getUrl('*/*/product', array('_current' => true)),
            'class' => 'ajax',
        ));

        $this->addTab('dailydeal', array(
            'label' => Mage::helper('dailydeal')->__('Information'),
            'alt' => Mage::helper('dailydeal')->__('Information'),
            'content' => $this->getLayout()->createBlock('dailydeal/adminhtml_dailydeal_edit_tab_dailydeal')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
