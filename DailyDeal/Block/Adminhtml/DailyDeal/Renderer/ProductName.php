<?php

class Tigren_DailyDeal_Block_Adminhtml_DailyDeal_Renderer_ProductName extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $product = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addAttributeToFilter('entity_id', $row->getEntityId());
        $name = "";
        foreach ($product as $item)
            $name = $item->getData();
        return $name['name'];
    }
}