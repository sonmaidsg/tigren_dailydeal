<?php

class Tigren_DailyDeal_Block_Adminhtml_DailyDeal_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct() {
        parent::__construct();

        $this->_objectId = 'deal_id';
        $this->_blockGroup = 'dailydeal';
        $this->_controller = 'adminhtml_dailydeal';

        $this->_updateButton('save', 'label', Mage::helper('dailydeal')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('dailydeal')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label' => Mage::helper('dailydeal')->__('Save and continue'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText() {
        /** @var Tigren_DailyDeal_Model_DailyDeal $model */
        $model = Mage::registry('dailydeal_data');
        if ($model && $model->getId()) {
            return Mage::helper('dailydeal')->__('Edit Daily Deal');
        } else {
            return Mage::helper('dailydeal')->__('Add Daily Deal');
        }
    }

}
