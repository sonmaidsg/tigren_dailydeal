<?php

class Tigren_DailyDeal_Block_Adminhtml_DailyDeal_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('dailydealGrid');
        $this->setDefaultSort('deal_id');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
//        die(var_dump(Mage::getModel('dailydeal/dailydeal')->load(1)->getData()));
        $collection = Mage::getModel('dailydeal/dailydeal')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('deal_id', array(
            'header'    => 'ID',
            'width'     => '40',
            'index'     => 'deal_id',
            'type'  => 'number',
        ));

        $this->addColumn('deal_name', array(
            'header'    => 'Name',
            'width'     => '150',
            'index'     => 'deal_name',
            'type'  => 'text',
        ));

        $this->addColumn('entity_id', array(
            'header'    => 'Product',
            'width'     => '150',
            'index'     => 'entity_id',
            'type'  => 'text',
            'renderer' => 'dailydeal/adminhtml_dailydeal_renderer_productname',
        ));

        $this->addColumn('deal_price', array(
            'header'    => 'Deal Price',
            'width'     => '100',
            'index'     => 'deal_price',
            'type'  => 'price',
            'currency_code' => Mage::app()->getStore()->getBaseCurrency()->getCode()
        ));

        $this->addColumn('deal_price_type', array(
            'header'    => 'Type Price',
            'width'     => '100',
            'index'     => 'deal_price_type',
            'type' => 'options',
            'options' => ['0' => 'Sale Off', '1' => 'Price Only'],
        ));

        $this->addColumn('deal_qty', array(
            'header'    => 'Deal Quanlity',
            'width'     => '100',
            'index'     => 'deal_qty'
        ));

        $this->addColumn('deal_qty_sold', array(
            'header'    => 'Deal Quanlity Sold',
            'width'     => '100',
            'index'     => 'deal_qty_sold'
        ));

        $this->addColumn('deal_start', array(
            'header' => 'From',
            'type' => 'datetime',
            'index' => 'deal_start',
            'width' => '150',
            'timezone' => true,
        ));

        $this->addColumn('deal_end', array(
            'header' => 'End',
            'type' => 'datetime',
            'index' => 'deal_end',
            'width' => '150',
            'timezone' => true,
        ));


        $this->addColumn('status', array(
            'header'    => 'Status',
            'width'     => '150',
            'index'     => 'status',
            'type' => 'options',
            'options' => ['1' => 'Enable', '0' => 'Disabled'],
        ));

        return parent::_prepareColumns();
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('deal_id');
        $this->getMassactionBlock()->setFormFieldName('dailydeal');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => 'Delete',
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => ('Are you sure?')
        ));
        return $this;
    }


    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('deal_id'=>$row->getId()));
    }
}
