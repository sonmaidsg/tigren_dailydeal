<?php

class Tigren_DailyDeal_Block_Adminhtml_DailyDeal extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_blockGroup = 'dailydeal';
        $this->_controller = 'adminhtml_dailydeal';
        $this->_headerText = 'Daily Deal';

        parent::__construct();
    }

}
