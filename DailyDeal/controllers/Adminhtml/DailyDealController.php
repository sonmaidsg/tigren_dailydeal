<?php

class Tigren_DailyDeal_Adminhtml_DailyDealController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction(){
        echo '<pre>';
        $product = Mage::getModel('catalog/product')->load(232);
        die(print_r($product->getData()));
        $this->_title($this->__('Daily Deal'));
        $this->loadLayout();
        $this->_setActiveMenu('dailydeal/dailydeal');
        $this->_addBreadcrumb('Daily Deal', 'Daily Deal');
        $this->_addBreadcrumb('Manage Daily Deal', 'Manage Daily Deal');
        $this->renderLayout();
    }


    public function newAction(){
        $this->_forward('edit');
    }

    public function editAction() {
        $this->loadLayout();
        $this->_title($this->__('Edit'));
        $id = (int) $this->getRequest()->getParam('deal_id');
        $model = Mage::getModel('dailydeal/dailydeal');
        if($id) $model->load($id);
        Mage::register('current_dailydeal', $model);
        if($model->getId()){
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            Mage::register('dailydeal_data', $model);
        }
        $this->renderLayout();
    }


    public function saveAction() {

        if ($data = $this->getRequest()->getPost()) {

            $model = Mage::getModel('dailydeal/dailydeal');
            $model->setData($data)->setId($this->getRequest()->getParam('deal_id'));

            $links = $this->getRequest()->getPost('in_deal');
            $entityId = Mage::helper('adminhtml/js')->decodeGridSerializedInput($links);



            try {
                $model->setData('entity_id', $entityId[0]);
                $model->save();
                if($this->getRequest()->getParam('deal_id')){
                    Mage::getSingleton('adminhtml/session')->addSuccess('Saved successfully');
                } else {
                    Mage::getSingleton('adminhtml/session')->addSuccess('New data saved successfully');
                }
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('deal_id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('deal_id' => $this->getRequest()->getParam('deal_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError('Registry not found');
        $this->_redirect('*/*/');
    }

    public function productAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock('product.grid')
            ->setInDeal($this->getRequest()->getPost('in_deal', null));
        $this->renderLayout();
    }

    public function productgridAction(){
        $this->loadLayout();
        $this->getLayout()->getBlock('product.grid')
            ->setInDeal($this->getRequest()->getPost('in_deal', null));
        $this->renderLayout();
    }
//
//    public function deleteAction() {
//        if ($this->getRequest()->getParam('id') > 0) {
//            try {
//                $model = Mage::getModel('testimonials/testimonials')->load($this->getRequest()->getParam('id'));
//                $model->delete();
//                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Testimonial removed'));
//                $this->_redirect('*/*/');
//            } catch (Exception $e) {
//                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
//                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
//            }
//        }
//        $this->_redirect('*/*/');
//    }
//
//    public function massDeleteAction() {
//        $ids = $this->getRequest()->getParam('testimonials');
//        if (!is_array($ids)) {
//            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please, select an item'));
//        } else {
//            try {
//                $removed = array();
//                foreach ($ids as $id) {
//                    $model = Mage::getModel('testimonials/testimonials')->load($id);
//                    if($model->getId()){
//                        $removed[] = $model->getId();
//                        $model->delete();
//                    }
//
//                }
//
//                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d item(s) removed', count($removed)));
//            } catch (Exception $e) {
//                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
//            }
//        }
//        $this->_redirect('*/*/index');
//    }

}
