<?php

class Tigren_DailyDeal_Model_Resource_DailyDeal extends Mage_Core_Model_Resource_Db_Abstract
{
    protected $_dailydealStoreTable;

    protected function _construct()
    {
        $this->_init('dailydeal/dailydeal', 'deal_id');
        $this->_dailydealStoreTable = $this->getReadConnection()->getTableName('tigren_daily_deal_store');
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        parent::_afterLoad($object);
        if (!$object->getId()) {
            return $this;
        }

        $object->setStores($this->getStores($object->getId()));

        return $this;
    }


    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if ($object->hasData('stores') && is_array($object->getStores())) {
            $stores = $object->getStores();
            $object->setStores($stores);
        } elseif ($object->hasData('stores')) {
            $object->setStores($object->getStores());
        } elseif (!$object->hasData('stores')) {
            $object->setStores(Mage::app()->getStore()->getStoreId());
        }

        return $this;
    }

    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {

        $connection = $this->getReadConnection();

        $stores = $object->getStores();
        if (!empty($stores)) {
            $condition = ['deal_id = ?' => $object->getId()];
            $connection->delete($this->_dailydealStoreTable, $condition);

            $insertedStoreIds = [];
            foreach ($stores as $storeId) {
                if (in_array($storeId, $insertedStoreIds)) {
                    continue;
                }

                $insertedStoreIds[] = $storeId;
                $storeInsert = ['store_id' => $storeId, 'deal_id' => $object->getId()];
                $connection->insert($this->_dailydealStoreTable, $storeInsert);
            }
        }

        return $this;
    }


    public function getStores($dailyDealId)
    {
        $select = $this->getReadConnection()->select()->from(
            $this->_dailydealStoreTable,
            'store_id'
        )->where(
            'deal_id = ?',
            $dailyDealId
        );
        return $this->getReadConnection()->fetchCol($select);
    }
}

?>