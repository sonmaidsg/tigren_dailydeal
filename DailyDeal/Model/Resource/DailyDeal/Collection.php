<?php

class Tigren_DailyDeal_Model_Resource_DailyDeal_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
    protected function _construct()
    {
        $this->_init('dailydeal/dailydeal');
    }

}