<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->getConnection()->dropTable($installer->getTable('tigren_daily_deal'));
$installer->getConnection()->dropTable($installer->getTable('tigren_daily_deal_store'));

$tableDailyDeal = $installer->getTable('tigren_daily_deal');
if(!$installer->tableExists($tableDailyDeal)){
    $table = $installer->getConnection()
        ->newTable($tableDailyDeal)
        ->addColumn('deal_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true,
        ), 'ID')
        ->addColumn(
            'deal_name',
            Varien_Db_Ddl_Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Deal Name'
        )
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned'  => true,
            'nullable'  => true,
            'default'   => null,
        ), 'Product ID')
        ->addColumn('deal_start', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array())
        ->addColumn('deal_end', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array())
        ->addColumn('deal_price_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'nullable' => false,
            'default' => '0'
        ), 'Deal Price Type')
        ->addColumn(
            'deal_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,1', array(
            'nullable' => true,
            'default' => null,
        ), "Deal Price")
        ->addColumn('deal_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => true,
            'default'   => '0',
        ), 'Quantily')
        ->addColumn('deal_qty_sold', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable'  => true,
            'default'   => '0',
        ), 'Quantily Sold')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'nullable' => false,
            'default' => '1'
        ), 'Status');
    $installer->getConnection()->createTable($table);
}

$tableDailyDealStore = $installer->getTable('tigren_daily_deal_store');
if(!$installer->tableExists($tableDailyDealStore)){
    $table = $installer->getConnection()
        ->newTable($tableDailyDealStore)
        ->addColumn(
            'deal_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => false,
                'primary'   => true,
            ],
            'Deal ID'
        )->addColumn(
            'store_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => false,
                'primary'   => true,
            ],
            'Store ID'
        )->addIndex(
            $installer->getIdxName('tigren_daily_deal_store', ['deal_id']),
            ['deal_id']
        )->addIndex(
            $installer->getIdxName('tigren_daily_deal_store', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName(
                'tigren_daily_deal_store',
                'deal_id',
                'tigren_daily_deal',
                'deal_id'
            ),
            'deal_id',
            $installer->getTable('tigren_daily_deal'),
            'deal_id',
            Varien_Db_Ddl_Table::ACTION_CASCADE,
            Varien_Db_Ddl_Table::ACTION_CASCADE
        );
        $installer->getConnection()->createTable($table);
}
$installer->endSetup();



//$installer = $this;
///* @var $installer Mage_Core_Model_Resource_Setup */
//$installer->startSetup();
//
//$installer->getConnection()->dropTable($installer->getTable('tigren_daily_deal'));
//$installer->getConnection()->dropTable($installer->getTable('tigren_daily_deal_store'));
//
//    $table = $installer->getConnection()->newTable(
//    $installer->getTable('tigren_daily_deal'))
//        ->addColumn('deal_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
//            'identity'  => true,
//            'unsigned'  => true,
//            'nullable'  => false,
//            'primary'   => true,
//        ), 'ID')
//        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
//            'unsigned'  => true,
//            'nullable'  => true,
//            'default'   => null,
//        ), 'Product ID')
//        ->addColumn('deal_start', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array())
//        ->addColumn('deal_end', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array())
//        ->addColumn(
//            'deal_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
//            'nullable' => true,
//            'default' => null,
//        ), "Deal Price")
//        ->addColumn('deal_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
//            'nullable'  => true,
//            'default'   => '0',
//        ), 'Quantily')
//        ->addColumn('deal_qty_sold', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
//            'nullable'  => true,
//            'default'   => '0',
//        ), 'Quantily Sold')
//        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
//            'nullable' => false,
//            'default' => '1'
//        ), 'Status')
//        ->addIndex(
//            $installer->getIdxName('tigren_daily_deal', ['deal_id']),
//            ['testimonial_id'])
//        ->addIndex(
//            $installer->getIdxName('tigren_daily_deal', ['status']),
//            ['status'])
//        ->addIndex(
//            $installer->getIdxName('tigren_daily_deal', ['deal_start']),
//            ['deal_start'])
//        ->addIndex(
//            $installer->getIdxName('tigren_daily_deal', ['deal_end']),
//            ['deal_end']
//        );
//    $installer->getConnection()->createTable($table);
//
//    $table = $installer->getConnection()->newTable(
//        $installer->getTable('tigren_daily_deal_store'))
//        ->addColumn(
//            'deal_id',
//            Varien_Db_Ddl_Table::TYPE_INTEGER,
//            null,
//            [
//                'unsigned' => true,
//                'nullable' => false,
//                'primary'   => true,
//            ],
//            'Deal ID'
//        )->addColumn(
//            'store_id',
//            Varien_Db_Ddl_Table::TYPE_INTEGER,
//            null,
//            [
//                'unsigned' => true,
//                'nullable' => false,
//                'primary'   => true,
//            ],
//            'Store ID'
//        )->addIndex(
//            $installer->getIdxName('tigren_daily_deal_store', ['deal_id']),
//            ['deal_id']
//        )->addIndex(
//            $installer->getIdxName('tigren_daily_deal_store', ['store_id']),
//            ['store_id']
//        )->addForeignKey(
//            $installer->getFkName(
//                'tigren_daily_deal_store',
//                'deal_id',
//                'tigren_daily_deal',
//                'deal_id'
//            ),
//            'deal_id',
//            $installer->getTable('tigren_daily_deal'),
//            'deal_id',
//            Varien_Db_Ddl_Table::ACTION_CASCADE,
//            Varien_Db_Ddl_Table::ACTION_CASCADE
//        );
//        $installer->getConnection()->createTable($table);
//
//$installer->endSetup();

