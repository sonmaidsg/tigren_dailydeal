<?php

class Tigren_DailyDeal_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get the Store Config of Daily Deal Module
     * @param string $key Key of the config
     * @param string $group Group of the config (tab in admin)
     * @return string|null
     */
    public function getConfig($key, $group = 'settings'){
        return Mage::getStoreConfig('dailydeal/'.$group.'/'.$key);
    }

    /**
     * Check if Module is active
     * @return bool
     */
    public function isActive(){
        return $this->getConfig('active');
    }

}
